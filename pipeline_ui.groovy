pipeline{
    agent {
        label 'jenkins_slave'
    }
    environment{
        workspace="/data/"
    }
    stages{
        stage("Limpiar"){
            steps{
                cleanWs()
            }
        }
        stage("download Project"){
            steps{
                git credentialsId: 'git_credentials', branch: "installation-view", url: "https://gitlab.com/sointelca/fibra-optica/sga-nets-frontend.git"
                echo "proyecto Frontend descargado"
                stash name: 'frontartifact'
                echo "proyecto Frontend guardado"
            }
        }
        stage('Image push artifactory')
        {
            agent {label 'docker_node'}
            steps{
                script{
                    unstash 'frontartifact'
                    sh "rm -r /data/proyects/ui/* | true"
                    sh "cp -r ./* /data/proyects/ui/"
                    sh "docker rmi jhudah/sga-frontend:latest | true; cd /data/proyects/ui/ ; docker build -t jhudah/sga-frontend:latest ."
                    sh "docker push jhudah/sga-frontend:latest "
                }
            }
        }
        stage('Build Backend')
        {
            agent {label 'docker_node'}
            steps{
                script{
                sh """
                    cd /data/service/ui/
                    pwd ; ls
                    docker-compose down | true 
                    docker-compose up -d 
                    """
                }
            }
        }
    }
}